import { Fragment } from 'react'
import Nav from './Nav'
import AttendeesList from './attendees/AttendeesList'
import LocationForm from './locations/new/LocationForm'
import ConferenceForm from './conferences/new/ConferenceForm'
import AttendeesConferenceForm from './attendees/new/AttendConferenceForm'
import PresentationForm from './presentations/new/PresentationForm'
import MainPage from './MainPage'
import { BrowserRouter, Routes, Route, } from 'react-router-dom'

function App(props) {
  if (props.attendees === undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
            <Route path='conferences'>
              <Route path='new'  element={<ConferenceForm />} />
            </Route>
            <Route path='locations'>
              <Route path='new' element={<LocationForm />} />
            </Route>
            <Route path='attendees'>
              <Route index element={<AttendeesList attendees={props.attendees} />} />
              <Route path='new' element={<AttendeesConferenceForm />} />
            </Route>
            <Route path='presentations'>
              <Route path='new' element={<PresentationForm /> } />
            </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
