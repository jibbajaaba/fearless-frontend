import React, { useState, useEffect } from 'react'

function PresentationForm() {
  const [conferences, setConferences] = useState([])
  const [conference, setConference] = useState('')
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [company, setCompany] = useState('')
  const [title, setTitle] = useState('')
  const [synopsis, setSynop] = useState('')

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setConferences(data.conferences)
    }
  }

  function conferenceChange(event) {
    setConference(event.target.value)
  }
  function nameChange(event) {
    setName(event.target.value)
  }

  function emailChange(event) {
    setEmail(event.target.value)
  }

  function companyChange(event) {
    setCompany(event.target.value)
  }

  function titleChange(event) {
    setTitle(event.target.value)
  }

  function synopChange(event) {
    setSynop(event.target.value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    const data = {}

    data.presenter_name = name
    data.company_name = company
    data.presenter_email = email
    data.title = title
    data.synopsis = synopsis
    data.conference = conference

    const formTag = document.getElementById('create-presentation-form')
    const selectTag = document.getElementById('conference')
    const conferenceId = selectTag.options[selectTag.selectedIndex].value
    const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }

    const response = await fetch(presentationUrl, fetchConfig)
    if (response.ok) {
      formTag.reset()
      const newPresentation = await response.json
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input onChange={nameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name"
                className="form-control" />
              <label htmlFor="presenter_name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={emailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email"
                className="form-control" />
              <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={companyChange} placeholder="Company name" type="text" name="company_name" id="company_name"
                className="form-control" />
              <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={titleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
              <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
              <textarea className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              <label onChange={synopChange} htmlFor="synopsis">Synopsis</label>
            </div>
            <div className="mb-3">
              <select onChange={conferenceChange} required name="conference" id="conference" className="form-select">
                <option value="">Choose a conference</option>
                {conferences.map(conference => {
                  return (
                    <option key={conference.id} value={conference.id}>
                      {conference.name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default PresentationForm
