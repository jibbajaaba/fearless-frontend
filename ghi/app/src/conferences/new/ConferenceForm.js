import React, { useEffect, useState } from "react"

function ConferenceForm() {
  const [locations, setLocations] = useState([])
  const [name, setName] = useState('')
  const [start, setStart] = useState('')
  const [end, setEnd] = useState('')
  const [textarea, setTextarea] = useState('')
  const [maxpresent, setMaxPresent] = useState('')
  const [maxattendees, setMaxAttendees] = useState('')
  const [location, setLocation] = useState('')

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setLocations(data.locations)
    }
  }

  function nameChange(event) {
    setName(event.target.value)
  }

  function startChange(event) {
    setStart(event.target.value)
  }

  function endChange(event) {
    setEnd(event.target.value)
  }

  function textareaChange(event) {
    setTextarea(event.target.value)
  }

  function maxPresentChange(event) {
    setMaxPresent(event.target.value)
  }

  function maxAttendeesChange(event) {
    setMaxAttendees(event.target.value)
  }

  function locationChange(event) {
    setLocation(event.target.value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    const data = {}


    data.name = name
    data.starts = start
    data.ends = end
    data.description = textarea
    data.max_presentations = maxpresent
    data.max_attendees = maxattendees
    data.location = location

    const conferenceUrl = 'http://localhost:8000/api/conferences/'
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const formTag = document.getElementById('create-conference-form')
    const response = await fetch(conferenceUrl, fetchConfig)
    if (response.ok) {
      formTag.reset()
      const newConference = await response.json()
    }
  }

  useEffect(() => {
    fetchData()
  }, [])
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={nameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={startChange} placeholder="mm/dd/yy" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="name">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={endChange} placeholder="mm/dd/yy" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="name">Ends</label>
            </div>
            <div className="form-floating mb-3">
              <textarea onChange={textareaChange} placeholder="" required name="description" id="description"
                className="form-control" cols="30" rows="10"></textarea>
              <label htmlFor="room_count">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={maxPresentChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="city">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={maxAttendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="city">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select onChange={locationChange} required id="location" name="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                        {location.name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ConferenceForm
