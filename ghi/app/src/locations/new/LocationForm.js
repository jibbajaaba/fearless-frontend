import React, { useEffect, useState } from "react";

function LocationForm(props) {
  const [states, setStates] = useState([])
  const [name, setName] = useState('')
  const [roomCount, setRoomCount] = useState('')
  const [city, setCity] = useState('')
  const [select, selectState] =useState('')



  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setStates(data.states)
    }
  }

  function handleNameChange(event) {
    setName(event.target.value)
  }

  function roomCountChange(event) {
    setRoomCount(event.target.value)
  }

  function cityChange(event) {
    setCity(event.target.value)
  }

  function selectChange(event) {
    selectState(event.target.value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    const data = {}

    data.room_count = roomCount
    data.name = name
    data.city = city
    data.state = select

    const locationUrl = 'http://localhost:8000/api/locations/'
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const formTag = document.getElementById('create-location-form')
    const response = await fetch(locationUrl, fetchConfig)
    if (response.ok) {
      formTag.reset()
      const newLocation = await response.json()
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text"
                name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={roomCountChange} placeholder="Room count" required type="number" name="room_count" id="room_count"
                  className="form-control"/>
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={cityChange} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange={selectChange} required id="state" name="state" className="form-select">
                  <option value="">Choose a state</option>
                  {states.map(state => {
                    return (
                      <option key={state.abbreviation} value={state.abbreviation}>
                        {state.name}
                      </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}

export default LocationForm
